﻿namespace todo_list.Models
{
    public class TodoItemDTO
    {
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
