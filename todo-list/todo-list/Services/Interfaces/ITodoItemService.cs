﻿using todo_list.Entities;
using todo_list.Models;

namespace todo_list.Services.Interfaces
{
    public interface ITodoItemService
    {
        int CreateTodoItem(TodoItem todoItem);
        TodoItem UpdateTodoItem(TodoItem todoItem);
        void DeleteTodoItem(int todoItemId);
        IEnumerable<TodoItemDTO> GetAllTodoItems();
    }
}
