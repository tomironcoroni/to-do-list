﻿using todo_list.Entities;

namespace todo_list.Services.Interfaces
{
    public interface IUserService
    {
        int CreateUser(User user);
        IEnumerable<User> GetAllUsers();
    }
}
