﻿using System;
using todo_list.DBContext;
using todo_list.Entities;
using todo_list.Models;
using todo_list.Services.Interfaces;

namespace todo_list.Services.Implementations
{
    public class TodoItemService : ITodoItemService
    {
        private readonly todolistContext _context;

        public TodoItemService(todolistContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public int CreateTodoItem(TodoItem todoItem)
        {
            if (todoItem == null)
            {
                throw new ArgumentNullException(nameof(todoItem));
            }

            _context.TodoItems.Add(todoItem);
            _context.SaveChanges();

            return todoItem.Id;
        }

        public TodoItem UpdateTodoItem(TodoItem todoItem)
        {
            if (todoItem == null)
            {
                throw new ArgumentNullException(nameof(todoItem));
            }

            var existingTodoItem = _context.TodoItems.Find(todoItem.Id);

            if (existingTodoItem == null)
            {
                throw new ArgumentException($"TodoItem with ID {todoItem.Id} not found.");
            }

            existingTodoItem.Title = todoItem.Title;
            existingTodoItem.Description = todoItem.Description;

            _context.SaveChanges();

            return existingTodoItem;
        }

        public void DeleteTodoItem(int todoItemId)
        {
            var todoItem = _context.TodoItems.Find(todoItemId);

            if (todoItem == null)
            {
                throw new ArgumentException($"TodoItem with ID {todoItemId} not found.");
            }

            _context.TodoItems.Remove(todoItem);
            _context.SaveChanges();
        }

        public IEnumerable<TodoItemDTO> GetAllTodoItems()
        {
            var todoItems = _context.TodoItems.ToList();
            var todoItemDtos = todoItems.Select(ti => new TodoItemDTO
            {
                UserId = ti.UserId,
                Title = ti.Title,
                Description = ti.Description
            });

            return todoItemDtos;
        }
    }
}
