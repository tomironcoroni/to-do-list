﻿using todo_list.DBContext;
using todo_list.Entities;
using todo_list.Services.Interfaces;

namespace todo_list.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly todolistContext _todolistContext;

        public UserService(todolistContext todolistContext)
        {
            _todolistContext = todolistContext;
        }

        public int CreateUser(User user)
        {
            _todolistContext.Add(user);
            _todolistContext.SaveChanges();
            return user.Id;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _todolistContext.Users.ToList();
        }
    }
}
