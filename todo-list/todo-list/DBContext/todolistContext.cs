﻿using Microsoft.EntityFrameworkCore;
using todo_list.Entities;

namespace todo_list.DBContext
{
    public class todolistContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }

        public todolistContext(DbContextOptions<todolistContext> dbContextOptions) : base(dbContextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoItem>()
               .HasOne(ti => ti.User)
               .WithMany(u => u.TodoItems)
               .HasForeignKey(ti => ti.UserId)
               .OnDelete(DeleteBehavior.Cascade);
            base.OnModelCreating(modelBuilder);
        }
    }
}
