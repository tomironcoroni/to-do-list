﻿using Microsoft.AspNetCore.Mvc;
using todo_list.Entities;
using todo_list.Models;
using todo_list.Services.Interfaces;

namespace todo_list.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult GetAllUsers()
        {
            var users = _userService.GetAllUsers();
            return Ok(users);
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] UserDTO userDto)
        {
            if (userDto == null)
            {
                return BadRequest("User data is null.");
            }

            // Convertir UserDto a User antes de crear el usuario en UserService
            var user = new User
            {
                Name = userDto.Name,
                Email = userDto.Email,
                Address = userDto.Address
            };

            int userId = _userService.CreateUser(user);
            return Ok(new { UserId = userId });
        }
    }
}
