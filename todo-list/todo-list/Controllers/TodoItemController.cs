﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using todo_list.Entities;
using todo_list.Models;
using todo_list.Services.Interfaces;

namespace todo_list.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemController : ControllerBase
    {
        private readonly ITodoItemService _todoItemService;

        public TodoItemController(ITodoItemService todoItemService)
        {
            _todoItemService = todoItemService;
        }

        [HttpGet]
        public IActionResult GetAllTodoItems()
        {
            var todoItems = _todoItemService.GetAllTodoItems();

            if (todoItems == null)
            {
                return NotFound("No todo items found.");
            }

            return Ok(todoItems);
        }

        [HttpPost]
        public IActionResult CreateTodoItem([FromBody] TodoItemDTO todoItemDto)
        {
            if (todoItemDto == null)
            {
                return BadRequest("TodoItem data is null.");
            }

            var todoItem = new TodoItem
            {
                UserId = todoItemDto.UserId,
                Title = todoItemDto.Title,
                Description = todoItemDto.Description
            };

            int todoItemId = _todoItemService.CreateTodoItem(todoItem);
            return Ok(new { TodoItemId = todoItemId });
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTodoItem(int id, [FromBody] TodoItemDTO todoItemDto)
        {
            if (todoItemDto == null || id != todoItemDto.UserId)
            {
                return BadRequest("Invalid data provided.");
            }

            var todoItem = new TodoItem
            {
                Id = id,
                UserId = todoItemDto.UserId,
                Title = todoItemDto.Title,
                Description = todoItemDto.Description
            };

            try
            {
                var updatedTodoItem = _todoItemService.UpdateTodoItem(todoItem);
                return Ok(updatedTodoItem);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTodoItem(int id)
        {
            try
            {
                _todoItemService.DeleteTodoItem(id);
                return Ok("TodoItem deleted successfully.");
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
